import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D
from matplotlib import cm
from loadmodel import loadmodel

fig = plt.figure()
ax = fig.add_subplot(121, projection='3d')
ay = fig.add_subplot(122, projection='3d')


grid_x, grid_y = np.mgrid[0:100000:101j, 0:20000:101j]
surf=6*( np.sqrt(grid_x+5e3) - np.sqrt(5e3) ) + 1
bed=np.zeros((np.shape(grid_x)))

ax.plot_surface(grid_x,grid_y,surf)
ax.plot_surface(grid_x,grid_y,bed)


md=loadmodel('/home/bfl022/Model/GHIP/hydro_intercomparison/basile-defleurian/valley/Models/E1Valley.nc')

# domain length
xend = 6.0e3
x = np.arange(0,xend+10,10)

# surf para
beta = 0.25
s2 = 100.0/xend
sx0 = -200.

# bed para
g1 = 0.5e-6
alpha = 3.

XPos, YPos = np.mgrid[0:6000:201j, -550:550:201j]

surf = 100.*(XPos+200.)**beta + s2*XPos - 100.*200.**beta
s_xend = np.nanmax(surf)


min_thick = 1.0
# bed parameters
bench_para = 300./xend
para=bench_para


# helper functions
f_func	= bench_para*XPos + XPos**2. * (s_xend-bench_para*6.0e3)/6.0e3**2.
g_func	= g1 * abs(YPos)**3.
h_func	= -4.5*(XPos/xend) + 5
#base


bed=f_func+ g_func * h_func

#bed[np.where((surf-bed)<-100)]=np.nan
surf=md.geometry.surface
lims=[np.nanmin(surf),np.nanmax(surf)]
norm=mpl.colors.Normalize(vmin=lims[0], vmax=lims[1])
cmap = plt.cm.viridis


#ay.plot_surface(XPos,YPos,bed,rstride=5,cstride=5)
ay.plot_trisurf(md.mesh.x,md.mesh.y,md.geometry.base,cmap=cmap,norm=norm,linewidth=0,alpha=1)
ay.plot_trisurf(md.mesh.x,md.mesh.y,surf,cmap=cmap,norm=norm,linewidth=0,alpha=1)
ay.set_xlim3d(0,6000)
ay.set_ylim3d(-600,600)
ay.set_zlim3d(0,600)
#ay.axis('equal')
