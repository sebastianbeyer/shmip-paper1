# Instructions to edit the latex source

The source is in the file SHMIP.tex, the references are in shmip.bib,
the figures are in the folder Figures/ and the supplementary material
is in the folder supplement/.

In order to make version control work, we put each sentence on its own
line: this way the diff shows which sentence changed.  Please keep it
this way when editing.

When adding to shmip.bib, please add to the end, use a key involving
the author(s) and year.

Happy SHMIPping!
